package com.example.sharedpreference

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.time.milliseconds

class MainActivity : AppCompatActivity() {

    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        sharedPreferences = getSharedPreferences("UserData", MODE_PRIVATE)
        save()
        read()
        delete()
    }

    fun save() {
        SaveButton.setOnClickListener(){
            val email = EmailEnter.text.toString()
            val firstname = FirstName.text.toString()
            val lastname = LastName.text.toString()
            val age = Age.text.toString()
            val address = Address.text.toString()
            if (email.isNotEmpty() && firstname.isNotEmpty() && lastname.isNotEmpty() &&
                age.isNotEmpty() && address.isNotEmpty()) {
                val edit = sharedPreferences.edit()
                edit.putString("Email", email)
                edit.putString("FirstName", firstname)
                edit.putString("LastName", lastname)
                edit.putString("Age", age)
                edit.putString("Address", address)
                edit.apply()
                Toast.makeText(this, "Information is saved", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
            }

        }


    }


        fun read() {
            ReadButton.setOnClickListener(){
                val email = sharedPreferences.getString("Email", "")
                val firstname = sharedPreferences.getString("FirstName", "")
                val lastname = sharedPreferences.getString("LastName", "")
                val age = sharedPreferences.getString("Age", "")
                val address = sharedPreferences.getString("Address", "")


                EmailEnter.setText(email)
                FirstName.setText(firstname)
                LastName.setText(lastname)
                Age.setText(age.toString())
                Address.setText(address) } }




        fun delete() {
            DeleteButton.setOnClickListener(){

            val edit = sharedPreferences.edit()
            edit.clear()
                Toast.makeText(this, "Information deleted", Toast.LENGTH_SHORT).show()
            edit.apply()

        }
    }
}